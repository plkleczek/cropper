% Cropper v1.0 (2016-04-17)
% -------------------------
%
% ABOUT
%
% A program to easily obtain cropped images of the given size. It was
% developed to facilitate the process of creating training and tests
% datasets for tasks of machine learning in the domain of image
% recognition.
%
% USAGE
%
% (preparation)
% - Open image from which you would like to take samples.
% - Specify the size of a sample.
% - Specify the directory in which samples will be saved.
% - Provide a filename pattern which sample files will follow. `%d` is a
% placeholder for file's ID (ID is an integer, usually with leading zeros).
% Positions specify the total number of digits in file's ID.
% Example: Assuming the "sample_%d_rgb" filename pattern and 3 positions,
% samples will be named as follows - sample_001_rgb, sample_002_rgb, ...
% The program automatically finds files in the target directory matching
% the given filename pattern and adjusts the currently assigned ID so that
% it is always a consecutive number.
% - Choose the file format of samples.
%
% (cropping)
% - Right-click on a full image. A preview of a sample will appear on the
% right side of program's window and the cropping region will be
% highlighted on the full image. If a sample would be taken too close to
% image borders (i.e. sample's area would exceed area of the image), no
% sample will be created and an appropriate message will be shown as a
% preview ("N/A, out of scope").
% - To save a preview to the file, right-click on the preview image.
%
% (navigation)
% - You may use standard zoom and pan tools from the toolbar.
% - You may also use keyboard shortcuts for same tasks:
%   [ or Num+ - zoom in
%   ] or Num- - zoom out
%   r - reset zoom to see the whole image
%   arrows - stepwise pan
%   p - activate pan (interactive mode)
%
% AUTHOR
%
% Pawel Kleczek (pawel.kleczek@agh.edu.pl)
% Faculty of Electrical Engineering, Automatics,
%    Computer Science and Biomedical Engineering
% AGH University of Science and Technology
%

function varargout = cropper(varargin)
% CROPPER MATLAB code for cropper.fig
%      CROPPER, by itself, creates a new CROPPER or raises the existing
%      singleton*.
%
%      H = CROPPER returns the handle to a new CROPPER or the handle to
%      the existing singleton*.
%
%      CROPPER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CROPPER.M with the given input arguments.
%
%      CROPPER('Property','Value',...) creates a new CROPPER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cropper_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cropper_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cropper

% Last Modified by GUIDE v2.5 17-Apr-2016 16:46:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cropper_OpeningFcn, ...
                   'gui_OutputFcn',  @cropper_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cropper is made visible.
function cropper_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cropper (see VARARGIN)

% Choose default command line output for cropper
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cropper wait for user response (see UIRESUME)
% uiwait(handles.canvasFigure);

% Orignal image
global I_org

% Cropped image.
global I_c

% Size of the cropped region [height x width]
global cropSize

% Path to the directory where the crops will be saved
global dirPath

% [y x] coordinates of the upper left corner of the cropping region in the
% coordinate system of the original image (i.e. image without padding)
global sampleCoords

% Extra padding added to an image so that its aspect ratio matches aspect
% ratio of its axes.
% [horizontal_padding, vertical_padding] (added from one side only)
global fullImagePadding


sampleCoords = [];
fullImagePadding = int32([0 0]);

clearAxes(handles.previewAxes, 'N/A');

hz = zoom;
setAllowAxesZoom(hz,gca,false)
hp = pan;
setAllowAxesPan(hp,gca,false)

I_org = imread('lena.bmp');

I_c = [];

cropSize = [32 32];
set(handles.widthEdit,'string', num2str(cropSize(2)))
set(handles.heightEdit,'string', num2str(cropSize(1)))

% Set default 'Save' options.
dirPath = pwd;

nPositions = 3;
set(handles.positionsEdit,'string', num2str(nPositions))

% Set icon.
jframe = get(handles.canvasFigure, 'javaframe');
jIcon = javax.swing.ImageIcon('cropper_icon.gif');
jframe.setFigureIcon(jIcon);

% Hack mode managers to preserve custom keyboard callbacks.
%
% Refer to:
%  http://undocumentedmatlab.com/blog/enabling-user-callbacks-during-zoom-pan
% hManager = uigetmodemanager(handles.canvasFigure);
% set(hManager.WindowListenerHandles, 'Enable', 'off');
% set(handles.canvasFigure, 'WindowKeyPressFcn', []);
% set(handles.canvasFigure, 'KeyPressFcn', @canvasFigure_KeyPressFcn);

% --- Outputs from this function are returned to the command line.
function varargout = cropper_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function pathPatternEdit_Callback(hObject, eventdata, handles)
% hObject    handle to pathPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pathPatternEdit as text
%        str2double(get(hObject,'String')) returns contents of pathPatternEdit as a double


% --- Executes during object creation, after setting all properties.
function pathPatternEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pathPatternEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in openImageButton.
function openImageButton_Callback(hObject, eventdata, handles)
% hObject    handle to openImageButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global I_org sampleCoords

[filename, user_canceled] = imgetfile;
if ~user_canceled
    I_org = imread(filename);
    sampleCoords = [];
    resizeFullImage(handles);
    clearAxes(handles.previewAxes, 'N/A');
end


function widthEdit_Callback(hObject, eventdata, handles)
% hObject    handle to widthEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of widthEdit as text
%        str2double(get(hObject,'String')) returns contents of widthEdit as a double

global cropSize

input = str2double(get(hObject,'string'));
if isnan(input)
    errordlg('You must enter a numeric value','Invalid Input','modal')
    uicontrol(hObject)
    return
else
    cropSize(2) = input;
end

% --- Executes during object creation, after setting all properties.
function widthEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to widthEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function heightEdit_Callback(hObject, eventdata, handles)
% hObject    handle to heightEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of heightEdit as text
%        str2double(get(hObject,'String')) returns contents of heightEdit as a double
global cropSize

input = str2double(get(hObject,'string'));
if isnan(input)
    errordlg('You must enter a numeric value','Invalid Input','modal')
    uicontrol(hObject)
    return
else
    cropSize(1) = input;
end


% --- Executes during object creation, after setting all properties.
function heightEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to heightEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in fileFormatMenu.
function fileFormatMenu_Callback(hObject, eventdata, handles)
% hObject    handle to fileFormatMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns fileFormatMenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from fileFormatMenu


% --- Executes during object creation, after setting all properties.
function fileFormatMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fileFormatMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function imageAxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate imageAxes


% --- Executes on mouse press over axes background.
function imageAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to imageAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

selectionType = get(handles.canvasFigure,'selectionType');

global I_c cropSize sampleCoords fullImagePadding I_org

if strcmp(selectionType, 'alt')
    currentPoint = get(gca,'Currentpoint');
    % [y x]
    currentPoint = floor([currentPoint(1,2) currentPoint(1,1)]);
    
    offset = [fullImagePadding(2) fullImagePadding(1)];
    % Coordinates in coordinate system of the original image.
    leftUpperCorner = currentPoint - floor(cropSize / 2) - offset;
    rightBottomCorner = leftUpperCorner + cropSize - 1;
%     proposedSampleCoords = [leftUpperCorner(1) - fullImagePadding(2), leftUpperCorner(2) - fullImagePadding(1)];
    
    s = size(I_org);
    if any([bsxfun(@le, leftUpperCorner, [0 0]) bsxfun(@gt, rightBottomCorner, s(1:2))])
        clearAxes(handles.previewAxes, {'N/A' '(out of scope)'});
    else
        sampleCoords = leftUpperCorner;
        %[leftUpperCorner(1) - fullImagePadding(2), leftUpperCorner(2) - fullImagePadding(1)];
        
        %% Show preview.
        I_c = I_org(int32(leftUpperCorner(1):rightBottomCorner(1)), int32(leftUpperCorner(2):rightBottomCorner(2)), :);
        h_Ip = imshow(I_c,'Parent', handles.previewAxes);
        set(h_Ip, 'ButtonDownFcn', {@previewAxes_ButtonDownFcn, handles});
        
        highlightCroppingRegion(handles)
    end
end

function highlightCroppingRegion(handles)
global cropSize sampleCoords I_f fullImagePadding

if isempty(sampleCoords)
    return
end

% Save current zoom of full image.
xlims = xlim(handles.imageAxes);
ylims = ylim(handles.imageAxes);

% [x y width height].
sampleCoordsFullImage = [sampleCoords(1) + fullImagePadding(2), sampleCoords(2) + fullImagePadding(1)];
rectangle = int32([sampleCoordsFullImage(2) sampleCoordsFullImage(1) cropSize(2) cropSize(1)]);

I_hl = insertShape(I_f, 'FilledRectangle',rectangle, 'Color','green', 'Opacity', 0.15);
I_hl = insertShape(I_hl, 'Rectangle',rectangle, 'Color','green', 'LineWidth', 2);

h_I = imshow(I_hl,'Parent', handles.imageAxes);
set(h_I, 'ButtonDownFcn', {@imageAxes_ButtonDownFcn, handles});        

% Restore zoom of full image.
xlim(handles.imageAxes, xlims);
ylim(handles.imageAxes, ylims);

% --- Executes on key press with focus on widthEdit and none of its controls.
function widthEdit_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to widthEdit (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over axes background.
function previewAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to previewAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global I_c dirPath

selectionType = get(handles.canvasFigure,'selectionType');

if strcmp(selectionType, 'alt') && ~isempty(I_c)
    filenamePattern = get(handles.pathPatternEdit,'String');

    fileExtensionList = get(handles.fileFormatMenu, 'String');
    selectedExtensionInx = get(handles.fileFormatMenu, 'Value');
    fileExtension = lower(fileExtensionList{selectedExtensionInx});
    
    nPositions =  str2double(get(handles.positionsEdit,'string'));
    
    % Check the recent file index

    numberSpecifier = sprintf('\\d{%d}', nPositions);
    basenameMask = strrep(filenamePattern, '%d', numberSpecifier);
    % Replace %d with \d{n}
    filenameSearchMask = sprintf('%s.%s', basenameMask, fileExtension);

    fileList = regexpdir(dirPath, filenameSearchMask, false);
    % Get ID of each file (i.e. the number without leading zeros).
    fileIds = cellfun(@(filename) str2double(regexp(filename, numberSpecifier, 'match')), fileList);
    % Compute ID of the currently saved crop.
    nextId = max(fileIds) + 1;
    if isempty(nextId)
        nextId = 1;
    end
    
    savedNumberSpecifier = sprintf('%%0%dd',nPositions);
    savedId = sprintf(savedNumberSpecifier, nextId);
    savedBasename = strrep(filenamePattern, '%d', savedId);
    
    savePath = sprintf('%s/%s.%s', dirPath, savedBasename, fileExtension);
    
    imwrite(I_c, savePath);
    fprintf('Saved as: %s\n', savePath)
end


% --- Executes on button press in openDirButton.
function openDirButton_Callback(hObject, eventdata, handles)
% hObject    handle to openDirButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global dirPath

newDir = uigetdir(dirPath);
if newDir ~= 0
    dirPath = newDir;
    setDirPath(handles.dirText);
end



function positionsEdit_Callback(hObject, eventdata, handles)
% hObject    handle to positionsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of positionsEdit as text
%        str2double(get(hObject,'String')) returns contents of positionsEdit as a double


% --- Executes during object creation, after setting all properties.
function positionsEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to positionsEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function clearAxes(hObject, message)
    cla(hObject)
    
    imshow([], 'Parent', hObject)

    xlim(hObject, [0 1]);
    ylim(hObject, [0 1]);
%     box(hObject, 'on')

    axes(hObject)

    set(gca,'box','on')
    set(gca, 'xtick',[], 'ytick',[]);
    
    if exist('message', 'var')
        t = text(0.5,0.5,message);
        set(t, 'HorizontalAlignment', 'center')
        set(t, 'Parent', hObject)
    end
    
    global sampleCoords
    sampleCoords = [];


% --- Executes on key press with focus on canvasFigure and none of its controls.
function canvasFigure_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to canvasFigure (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

global I

zoomFactor = 1.5;

h = handles.imageAxes;
xlims = xlim(h);
ylims = ylim(h);
dx = diff(xlims);
dy = diff(ylims);
moveFactor = 0.3;

switch eventdata.Key
    case {'add', 'leftbracket'}
        zoom(handles.imageAxes, zoomFactor);
    case {'subtract', 'rightbracket'}
        zoom(handles.imageAxes, 1/zoomFactor);
    case 'r'
        zoom out

    case 'p'
        pan
        
    case 'rightarrow'
        xlimRight = min(xlims(2) + dx * moveFactor, size(I,2));
        xlimLeft = xlimRight - dx;
        xlim(h, [xlimLeft xlimRight]);
    case 'leftarrow'
        xlimLeft = max(xlims(1) - dx * moveFactor, 1);
        xlimRight = xlimLeft + dx;
        xlim(h, [xlimLeft xlimRight]);

    case 'downarrow'
        ylimRight = min(ylims(2) + dy * moveFactor, size(I,1));
        ylimLeft = ylimRight - dy;
        ylim(h, [ylimLeft ylimRight]);
    case 'uparrow'
        ylimLeft = max(ylims(1) - dy * moveFactor, 1);
        ylimRight = ylimLeft + dy;
        ylim(h, [ylimLeft ylimRight]);
        
    case 'c'
        % TODO: center on selection (zoom accordingly)
        disp('')
end


% --- Executes when canvasFigure is resized.
function canvasFigure_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to canvasFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global cropSize maxCharactersToDisplayInPath

% Padding near figure's edges.
framePadding = 15; % [px]

% Height of a static texts consistng of one row.
staticTextRowHight = 15; % [px]

% Gap between full-image axes and preview axes.
interImageGap = 15; % [px]

% Gap between settings and full-image axes.
settingsFullImageGap = 20; % [px]

%
cropSizePanelWidth = 135;
cropSizePanelHeight = 70;

%
savePanelHeight = 105;

% Height of the whole setting row.
settingsRowHeight = savePanelHeight; % [px]

% Inter element gap.
interElementGap = 10; % [px]

%
oneRowButtonHeight = 22;

editTextHeight = 20;

popupMenuHeight = 23;

% Preview axes size range.
previewWidthMin = 100;
previewWidthMax = 300;
previewHeightMin = 100;
previewHeightMax = 300;

canvasWidthMin = 700;
canvasHeightMin = 450;

maxCharactersToDisplayInPath = 40;
pixelsPerChar = 5;

% --

figPos = get(handles.canvasFigure, 'Position');
figX = figPos(1);
figWidth = max(figPos(3), canvasWidthMin);
figHeight = max(figPos(4), canvasHeightMin);
figY = figPos(2) + figPos(4) - figHeight;
set(handles.canvasFigure, 'Position',[figX figY figWidth figHeight]);


cropSizePanelX = framePadding;
cropSizePanelY = framePadding;
set(handles.cropSizePanel, 'Position',[cropSizePanelX cropSizePanelY cropSizePanelWidth cropSizePanelHeight]);

savePanelX = cropSizePanelX + cropSizePanelWidth + interElementGap;
savePanelY = framePadding;
% TODO: make it dependent on size of elements in this panel
savePanelWidth = 355;
set(handles.savePanel, 'Position',[savePanelX savePanelY savePanelWidth savePanelHeight]);

openImageButtonWidth = 70;
openImageButtonX = cropSizePanelX + (cropSizePanelWidth - openImageButtonWidth) / 2;
% TODO: make it dependent on settingRowHeigh (in the middle of this gap)
openImageButtonY = cropSizePanelY + cropSizePanelHeight + interElementGap;
set(handles.openImageButton, 'Position',[openImageButtonX openImageButtonY openImageButtonWidth oneRowButtonHeight]);

maxPreviewZoom = 3;

previewAxesWidth = max(previewWidthMin, cropSize(1) * maxPreviewZoom);
previewAxesWidth = min(previewWidthMax, previewAxesWidth);
previewAxesHeight = max(previewHeightMin, cropSize(2) * maxPreviewZoom);
previewAxesHeight = min(previewHeightMax, previewAxesHeight);

previewAxesX = figWidth - framePadding - previewAxesWidth;
% TODO: temporary
previewAxesY = figHeight - framePadding - staticTextRowHight - interElementGap - previewAxesHeight;
set(handles.previewAxes, 'Position',[previewAxesX previewAxesY previewAxesWidth previewAxesHeight]);

imageAxesX = framePadding;
imageAxesY = settingsRowHeight + settingsFullImageGap;
imageAxesHeigt = figHeight - imageAxesY - interElementGap - staticTextRowHight - framePadding;
imageAxesWidth = figWidth - imageAxesX - (figWidth - previewAxesX) - interImageGap;
set(handles.imageAxes, 'Position',[imageAxesX imageAxesY imageAxesWidth imageAxesHeigt]);

fullImageTextX = framePadding;
fullImageTextY = figHeight- framePadding - staticTextRowHight;
set(handles.fullImageText, 'Position',[fullImageTextX fullImageTextY imageAxesWidth staticTextRowHight]);

fullImageTextX = framePadding;
fullImageTextY = figHeight - framePadding - staticTextRowHight;
set(handles.fullImageText, 'Position',[fullImageTextX fullImageTextY imageAxesWidth staticTextRowHight]);

previewTextY = previewAxesY + previewAxesHeight + interElementGap;
set(handles.previewText, 'Position',[previewAxesX previewTextY previewAxesWidth staticTextRowHight]);


authorNoteTextX = savePanelX + savePanelWidth + interElementGap;
authorNoteTextY = savePanelY;
authorNoteTextW = 170;
authorNoteTextH = savePanelHeight - 30;
set(handles.authorNoteText, 'Position',[authorNoteTextX authorNoteTextY authorNoteTextW authorNoteTextH]);



% -- Save options

positionsTextX = interElementGap;
positionsTextY = interElementGap;
positionsTextW = 54;
positionsTextH = staticTextRowHight;
set(handles.positionsText, 'Position',[positionsTextX positionsTextY positionsTextW positionsTextH]);

positionsEditX = positionsTextX + positionsTextW + interElementGap;
positionsEditY = positionsTextY;
positionsEditW = 40;
positionsEditH = editTextHeight;
set(handles.positionsEdit, 'Position',[positionsEditX positionsEditY positionsEditW positionsEditH]);

positionsRowHeight = max([positionsTextH positionsEditH]);

filenamePatternTextX = positionsTextX;
filenamePatternTextY = positionsTextY + positionsRowHeight + interElementGap;
filenamePatternTextW = 86;
filenamePatternTextH = staticTextRowHight;
set(handles.filenamePatternText, 'Position',[filenamePatternTextX filenamePatternTextY filenamePatternTextW filenamePatternTextH]);

pathPatternEditX = filenamePatternTextX + filenamePatternTextW + interElementGap;
pathPatternEditY = filenamePatternTextY;
pathPatternEditW = 20 * pixelsPerChar;
pathPatternEditH = editTextHeight;
set(handles.pathPatternEdit, 'Position',[pathPatternEditX pathPatternEditY pathPatternEditW pathPatternEditH]);

fileFormatMenuX = pathPatternEditX + pathPatternEditW + interElementGap;
fileFormatMenuY = filenamePatternTextY;
fileFormatMenuW = 60;
fileFormatMenuH = popupMenuHeight;
set(handles.fileFormatMenu, 'Position',[fileFormatMenuX fileFormatMenuY fileFormatMenuW fileFormatMenuH]);

filenameRowHeight = max([filenamePatternTextH pathPatternEditH positionsEditH]);

dirTextLabelX = positionsTextX;
dirTextLabelY = filenamePatternTextY + filenameRowHeight + interElementGap;
dirTextLabelW = 52;
dirTextLabelH = staticTextRowHight;
set(handles.dirTextLabel, 'Position',[dirTextLabelX dirTextLabelY dirTextLabelW dirTextLabelH]);

dirTextX = dirTextLabelX + dirTextLabelW + interElementGap;
dirTextY = dirTextLabelY;
dirTextW = maxCharactersToDisplayInPath * pixelsPerChar;
dirTextH = staticTextRowHight;
set(handles.dirText, 'Position',[dirTextX dirTextY dirTextW dirTextH]);

openDirButtonW = 60;
openDirButtonX = dirTextX + dirTextW + interElementGap;
openDirButtonY = dirTextLabelY;
openDirButtonH = oneRowButtonHeight;
set(handles.openDirButton, 'Position',[openDirButtonX openDirButtonY openDirButtonW openDirButtonH]);

% --

% Abbreviate path if it is too long.
setDirPath(handles.dirText);

resizeFullImage(handles);


function resizeFullImage(handles)
    global I_org I_f fullImagePadding

    imageAxesPosition = get(handles.imageAxes, 'Position');
    
    imageAxesWidth = imageAxesPosition(3);
    imageAxesHeigt = imageAxesPosition(4);
    
    axesAspectRatio = imageAxesWidth / imageAxesHeigt;

    imW = size(I_org,2);
    imH = size(I_org,1);
    imageAspectRatio = imW / imH;

    % Add such a padding that the displayed full image aspect ratio matches
    % aspect ratio of axes.
    if imageAspectRatio > axesAspectRatio
        % Add height.
        fimH = ceil(imW / axesAspectRatio);
        % Make sure the total passing is an even number (i.e. same samout
        % of padding is added from both sides of an image).
        fimH = fimH + mod(fimH - imH, 2);
        fimY = ceil((fimH - imH) / 2);
        I_f = uint8(zeros(fimH, imW, size(I_org,3)));
        I_f(fimY:(fimY+imH-1),:,:) = I_org;
        fullImagePadding = [0 (fimH - imH)/2];
    elseif imageAspectRatio < axesAspectRatio
        % Add width.
        fimW = ceil(imH * axesAspectRatio);
        % Make sure the total passing is an even number (i.e. same samout
        % of padding is added from both sides of an image).
        fimW = fimW + mod(fimW - imW, 2);
        fimX = ceil((fimW - imW) / 2);
        I_f = uint8(zeros(imH, fimW, size(I_org,3)));
        I_f(:,fimX:(fimX+imW-1),:) = I_org;
        fullImagePadding = [(fimW - imW)/2 0];
    end

    h_I = imshow(I_f,'Parent', handles.imageAxes);
    set(h_I, 'ButtonDownFcn', {@imageAxes_ButtonDownFcn, handles});
    
    highlightCroppingRegion(handles)


function setDirPath(handle)
    global dirPath maxCharactersToDisplayInPath

    pathText = dirPath;
    if length(dirPath) > maxCharactersToDisplayInPath
        fileSeparator = '\';
        fileSeparatorPoss = strfind(pathText, fileSeparator);
        firstChunk = pathText(1:(fileSeparatorPoss(1)));
        lastChunk = pathText((fileSeparatorPoss(end)):end);

        % Abbreviate intermediate folders.
        proposedPath = sprintf('%s...%s', firstChunk, lastChunk);

        % Abbreviate the ending of the target directory.
        if length(proposedPath) > maxCharactersToDisplayInPath
            proposedPath = [proposedPath(1:maxCharactersToDisplayInPath-3) '...'];
        end

        pathText = proposedPath;
    end

    set(handle, 'String',pathText);
    set(handle,'tooltipString',dirPath);
